<?php
session_start();
include_once('../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\ProfilePicture\Utility;
use App\ProfilePicture\Message;

$profile_picture=new ImageUploader();
$allUser=$profile_picture->index();
//$active=$profile_picture->active();
//Utility::d($active);

?>

<!Doctype html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <center><h2>All Profile Pictures of User</h2></center>
    <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }?>
    </div>
    <br><br>
    <ul class="list-group">
        <li class="list-group-item list-group-item-info"><strong>Profile Picture:</strong></li>
        <?php foreach($allUser as $info){
            if(!is_null($info->active)){
                ?>
                <li class="list-group-item list-group-item"><img src="../Resources/Images/<?php echo $info->images ?>" alt="image" height="200" width="200" class="img-responsive"></li>
            <?php }
        } ?>
    </ul>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allUser as $info){
                $sl++;
                ?>
                <td><?php echo $sl?></td>
                <td><?php echo $info->id?></td>
                <td><?php echo $info->name?></td>
                <td><img src="../Resources/Images/<?php echo $info->images?>" alt="image" height="100" width="100" class="img-responsive"></td>
                <td><?php if(is_null($info->active)){?>
                    <a href="active.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Make Active</a>
                    <?php }else {?>
                    <a href="deactive.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Make Deactive</a>
                    <?php }?>
                    <a href="view.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info->id?>" class="btn btn-success" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button" >Delete</a>

                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>