<?php

include_once('../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\ProfilePicture\Utility;
use App\ProfilePicture\Message;

$profile_picture=new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem=$profile_picture->view();

unlink($_SERVER['DOCUMENT_ROOT'].'/AfiyaAyman_Labexam7/Resources/Images/'.$singleItem->images);

$profile_picture->delete();