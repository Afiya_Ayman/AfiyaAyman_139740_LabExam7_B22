<?php
include_once('../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\ProfilePicture\Utility;
use App\ProfilePicture\Message;

$profile_picture=new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem=$profile_picture->view();
//Utility::dd($singleItem);
?>

<!Doctype html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h2>create Profile</h2>
    <form role="form" method="post" action="update.php" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $singleItem->id?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $singleItem->name?>">
        </div>
        <div class="form-group">
            <label>Picture</label>
            <input type="file" class="form-control" name="image">
            <img src="../Resources/Images/<?php echo $singleItem->images ?>" alt="image" height="200" width="200" class="img-responsive">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
</body>
</html>
