<?php

include_once('../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\ProfilePicture\Utility;
use App\ProfilePicture\Message;

$profile_picture=new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem=$profile_picture->view();
//Utility::dd($singleItem);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>User View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br><br>
<div class="container">
    <center><h2>View User from List</h2></center>
    <br><br>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success">ID:<strong><?php echo "   ".$singleItem->id ?></strong></li>
        <li class="list-group-item list-group-item-success">Name:<strong><?php echo "   ".$singleItem->name ?></strong></li>
        <li class="list-group-item list-group-item-success">Profile Picture:<img src="../Resources/Images/<?php echo $singleItem->images ?>" alt="image" height="200" width="200" class="img-responsive"></li>
    </ul>
</div>

</body>
</html>


