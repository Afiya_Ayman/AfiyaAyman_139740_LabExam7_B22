<?php

include_once('../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\ProfilePicture\Utility;
use App\ProfilePicture\Message;

$profile_picture=new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem=$profile_picture->deactive();