<?php
namespace App\ProfilePicture;
class ImageUploader
{
    public $id;
    public $name;
    public $image;
    public $conn;
    public $active;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","aymanlabexam7") or die("DB Failed");
    }

    public function prepare($data="")
    {
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if (array_key_exists('image',$data))
        {
            $this->image=$data['image'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `profilepicture`(`name`,`images`) VALUES ('".$this->name."','".$this->image."')";
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> User has been Stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }

    public function index()
    {
        $_allUser=array();
        $query="SELECT * FROM `profilepicture`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function view()
    {
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
    public function update()
    {
        if(!empty($this->image))
        {
            $query="UPDATE `profilepicture` SET `name`='".$this->name."',`images`= '".$this->image."' WHERE `id` =".$this->id;

        }
        else{
            $query="UPDATE `profilepicture` SET `name`='".$this->name."' WHERE `id` =".$this->id;
        }
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Success!</strong> User has been Updated successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }


    }
    public function delete()
    {
        $query="DELETE FROM `profilepicture` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-danger\">
        <strong>Success!</strong> User has been Deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }
    public function active()
    {
        $query="UPDATE `profilepicture` SET `active`='".time()."' WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
       if($result)
       {
           Utility::redirect("index.php");
       }


    }
    public function deactive()
    {
        $query="UPDATE `profilepicture` SET `active`= NULL WHERE `id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Utility::redirect("index.php");
        }

    }

}